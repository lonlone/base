# Install an xinetd configuration fragment.  This definition understands the
# possible configuration directives we've needed so far and assembles a
# fragment from a template, allowing the configuration to put directly into
# the Puppet manifest and selectively overridden without having to keep
# multiple copies of a file.
#
# Syntax:
#
#     base::xinetd::config { '<name>':
#         ensure      => present | absent,
#         service     => '<service>',
#         port        => '<port>',
#         flags       => '<flag> ...',
#         server_type => '<type> ...',
#         server      => '<path-to-server>',
#         server_args => '<arg> ...',
#         description => '<description>',
#         protocol    => 'tcp' | 'udp',
#         user        => '<user>',
#         group       => '<group>',
#         cps         => '<cps>',
#         per_source  => '<per-source>',
#         instances   => '<instances>',
#         log_type    => '<log_type>',
#         env         => '<setting> ...',
#     }
#
# All options other than server are optional.  Either server or a server_type
# parameter including INTERNAL must be provided.  server_type corresponds to
# the xinetd setting type (type is a reserved word in Puppet).
#
# If <name> does not match the /etc/services entry, set service to the
# /etc/services entry.  This should only be done when multiple configurations
# for the same service name are required, and will also set id.
#
# The options generally match the same keys in xinetd.conf files (so see the
# xinetd.conf(5) man page), except for log_type, which controls only the
# facility.  Logging is always forced to syslog, so omit that part of the
# configuration.  You should usually omit port unless you're configuring a
# service that has no /etc/services entry, but it may be required when
# configuring a service on both TCP and UDP.
#
# socket_type will be automatically switched to dgram and wait enabled if the
# protocol type is set to 'udp'.  You must explicitly set protocol to 'udp'
# for UDP-based services, even if that's the default socket type for that
# service, for the generated configuration to be correct.

define base::xinetd::config(
    $ensure      = 'present',
    $service     = $name,
    $server      = '',
    $server_args = '',
    $description = 'Managed by Puppet',
    $server_type = '',
    $protocol    = '',
    $port        = '',
    $flags       = '',
    $user        = 'root',
    $group       = 'root',
    $cps         = '',
    $per_source  = '',
    $instances   = '',
    $log_type    = '',
    $env         = ''
) {
    if ($server == '') and ($server_type !~ /INTERNAL/) {
        fail("one of server or server_type => 'INTERNAL' must be given")
    }

    # Make sure xinetd is installed
    include base::xinetd

    file { "/etc/xinetd.d/$name":
        ensure  => $ensure,
        content => template('base/xinetd/config.erb'),
        notify  => Service['xinetd'],
    }
}
