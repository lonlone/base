# Configure a remctl server.
#
# The client installation is handled separately.

class base::remctl {
  package { 'remctl-server': ensure => installed }

  # Install the xinetd configuration.  Force a PATH setting since remctld
  # scripts may be assuming they have a sane PATH but xinetd may not have
  # any PATH set.
  base::xinetd::config { 'remctl':
    server      => '/usr/sbin/remctld',
    description => 'Remote authenticated command server',
    cps         => '100 5',
    env         => 'PATH=/sbin:/bin:/usr/sbin:/usr/bin',
    require     => Package['remctl-server'],
  }

  # Switch RH boxes to 4373
  # TODO: remove this after the migration is complete and all systems are
  # on a modern enough branch to have picked up the change.
  if $::osfamily == 'RedHat' {
    base::filter { '/etc/services':
      regex   => 'remctl[ \t]*4444/tcp',
      value   => 'remctl    4373/tcp',
      require => Package['remctl-server'],
      notify  => Service['xinetd'],
    }
  }

  # Our default iptables rules for remctl opens connections to all of
  # Stanford.  This is mostly for legacy reasons, since it historically had
  # always done this and some systems provide remctl services to campus
  # (such as wallet).
  #
  # Individual groups should provide their own local override class that
  # restricts remctl to their bastion host and/or subnets from which their
  # administrators do work and use that class on most of their systems.
  base::iptables::rule { 'remctl':
    description => 'Allow remctl connections from all Stanford subnets',
    source      => [ '10.0.0.0/8',
                     '68.65.160.0/20',
                     '128.12.0.0/16',
                     '134.79.0.0/16',
                     '171.64.0.0/14',
                     '172.16.0.0/12',
                     '192.168.0.0/16',
                     '204.63.224.0/21' ],
    protocol    => 'tcp',
    port        => 4373,
  }

  # Ignore standard remctl log messages.   This doesn't ignore any of the
  # remctl commands, just messages from starting or stopping the generic
  # daemon and messages caused by port scans or common client errors.
  file { '/etc/filter-syslog/remctl':
    source => 'puppet:///modules/base/remctl/etc/filter-syslog/remctl',
  }
}
