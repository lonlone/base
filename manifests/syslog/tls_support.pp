# Packages and rsyslog fragments required for TLS/RELP support.  This
# class is included from base::syslog::tls.

class base::syslog::tls_support {

  package {
    'rsyslog-gnutls': ensure => installed;
    'rsyslog-relp':   ensure => installed;
  }
  base::syslog::fragment { '05-modules-relp.conf':
    ensure  => present,
    source  => 'puppet:///modules/base/syslog/etc/rsyslog.d/05-modules-relp.conf';
  }
}
