# create syslog.conf

define base::syslog::config::syslog(
    $ensure         = 'present',
    $syslogserver   = undef,
    $fixrhel5syslog = undef,
    $source         = undef,
    $owner          = 'root',
    $group          = 'root',
    $mode           = '0644',
    $replace        = true,
) {
  if $source {
    $template = undef
  } else {
    $template = template('base/syslog/syslog.conf.erb')
  }
  file { $name:
    ensure  => $ensure,
    source  => $source,
    content => $template,
    owner   => $owner,
    group   => $group,
    mode    => $mode,
    replace => $replace,
    notify  => Service['syslog'],
  }
}