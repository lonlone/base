##############################################################################
# Create rsyslog.conf
##############################################################################
#
# This define is used by the base::syslog module.  It is not execpted
# to be used directly, but the actions of base::syslog overridden as
# needed.
#
# The use_ variables allow for the following transitions.
#
#  1. From an rsyslog configuration that pulls in an old syslog.conf
#     to a configuration that is all rsyslog and uses rsyslog
#     fragments.
#
#  2. From v5 to v7 of rsyslog.  To use the default v7 configuration
#     the syslog.conf transition must be complete.
#
# Example:
#
#  This example uses the default rsyslog version 7 configuration.
#
#  class s_ldap::conf::syslog_dev inherits base::syslog {
#    Base::Syslog::Config::Rsyslog['/etc/rsyslog.conf'] {
#      use_syslog_conf => false,
#      use_v5          => false,
#    }
#  }
#
# Note: There are situations where puppet does not process booleans
# correctly.  Specifically this was found to be the case with the
# use_syslog_conf variable.  Because of this it might be necessary to
# specify use_syslog_conf as a string, for example:
#
#   use_syslog_conf = 'true'
#
# The perferred method is to use a boolean, but if that does not work
# use a string value.

define base::syslog::config::rsyslog(
  $ensure          = 'present',
  $source          = undef,
  $owner           = 'root',
  $group           = 'root',
  $mode            = '0644',
  $syslog_server   = 'logsink.stanford.edu',
  $replace         = true,
  $use_syslog_conf = false,
  $use_default     = true,
  $use_v5          = false,
  $use_logsink_server = true,
) {

  if ($use_syslog_conf != 'true' and $use_syslog_conf != true) {
    if ($use_v5 == 'true' or $use_v5 == true) {
      $rsyslog_tmpl = 'etc/rsyslog.d/20-templates-v5.conf'
      $rsyslog_def  = 'etc/rsyslog.d/95-default-v5.conf.erb'
    } else {
      $rsyslog_tmpl = 'etc/rsyslog.d/20-templates.conf'
      $rsyslog_def  = 'etc/rsyslog.d/95-default.conf.erb'
    }
    # Always install templates.  They can be ignore if there are
    # not useful.
    file { '/etc/rsyslog.d/20-templates.conf':
      ensure  => present,
      source  => "puppet:///modules/base/syslog/$rsyslog_tmpl",
      owner   => $owner,
      group   => $group,
      mode    => $mode,
      notify  => Service['syslog'],
    }
    # Make sure there is a directory to hold disk based queues.
    # This directory is specified in the rsyslog configuration
    # with the $WorkDirectory directive.  This is not really
    # required because the package creates this directory, but
    # it is safer to make sure the directory exists.
    file { '/var/spool/rsyslog':
      ensure  => directory,
      owner   => $owner,
      group   => $group,
      notify  => Service['syslog'],
    }

    # Install the default catch all rule, or not, as desired.
    if ($use_default == 'true' or $use_default == true){
      file { '/etc/rsyslog.d/95-default.conf':
        ensure  => present,
        content => template("base/syslog/$rsyslog_def"),
        owner   => $owner,
        group   => $group,
        mode    => $mode,
        notify  => Service['syslog'],
      }
    } else {
      file { '/etc/rsyslog.d/95-default.conf':
        ensure  => absent,
        notify  => Service['syslog'],
      }
    }
  }

  # Template for the base rsyslog configuration.
  if $source {
    $template = undef
  } else {
    $template = template('base/syslog/rsyslog.conf.erb')
  }
  file { $name:
    ensure  => $ensure,
    source  => $source,
    content => $template,
    owner   => $owner,
    group   => $group,
    mode    => $mode,
    replace => $replace,
    notify  => Service['syslog'],
  }
}
