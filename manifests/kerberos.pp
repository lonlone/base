# Set up basic Kerberos configuration and allow logins via Kerberos rlogin and
# company.
#
# **********************************************************************
# NOTE: If you wish to override the file /etc/krb.conf in your own class,
# and you are using the "source" parameter, be sure to undefine the
# "content" parameter or you will get an error. Example:
#
#   class s_myclass {
#     include base::kerberos
#
#     File['/etc/krb5.conf'] {
#       source  => 'puppet:///modules/s_accounts/etc/krb5.conf',
#       content => undef,
#     }
#   }
#
# UPDATE: Rather than overriding this class, you may want to use the
# $source parameter defined below.
# **********************************************************************
#
#
# $krb_env:
#   Which kerberos environment to use. Must be one of:
#   'prod', 'uat', 'qa', or 'test'.
#   Default: 'prod'
#
# $prefer_tcp:
#   Normal kerberos traffic uses UDP, but some applications
#   (lookin' at you Java!) work better with TCP. Set this parameter to
#   "true" to force the client to prefer TCP to UDP.
#   Default: false
#
# $rdns_enabled:
#   If 'true' have the Kerberos client do a reverse DNS lookup on the
#   hostname when connecting to a server. This should be set to 'false' if
#   you want the client to be able to connect to services where the service
#   name's IP address PTR record may not match the hostname (e.g., for
#   services running in Amazon Web Services).
#   Default: true
#
# $source:
#   If you want to use your own /etc/krb5.conf file completely overriding
#   the one provided in this class, use this parameter.
#   Example:
#     class { 'base::kerberos':
#       source => 'puppet:///modules/s_accounts/etc/krb5.conf',
#     }
#
#   Note that we don't have an an analagous "content" parameter as calling
#   a template from a different module is not a good idea.
#   Default: undef

class base::kerberos(
  $prefer_tcp   = false,
  $krb_env      = 'prod',
  $rdns_enabled = true,
  $source       = undef,
){

  # We only allow the 'prod', 'uat', and 'test' environments.
  case $krb_env {
    'prod', 'uat', 'test', 'qa': {}
    default: { fail("unrecognized kerberos environment '${krb_env}'") }
  }

  case $::osfamily {
    'RedHat': {
      package { 'krb5-workstation': ensure => present }
    }
    'Debian': {
      # We intentionally don't do anything here currently since some systems
      # use MIT and some use Heimdal.  Eventually, this should be a
      # parameterized class that says what type of Kerberos to install.
    }
    default: {
      fail("unsupported OS ${::operatingsystem}")
    }
  }

  # Check to see if we are in Livermore right now
  if (   ip_in_cidr($::ipaddress, '204.63.224.0/21')
      or ip_in_cidr($::ipaddress, '172.20.224.0/21')
  ) {
    $drSite = true
  } else {
    $drSite = false
  }

  # Basic Kerberos configuration.
  if ($source) {
    file { '/etc/krb5.conf':
      source => $source,
    }
  } else {
    file { '/etc/krb5.conf':
      content => template('base/kerberos/krb5.conf.erb'),
    }
  }

}

# base::kerberos::dr is no longer needed, because it's functionality has been
# implemented in base::kerberos.
# Thie class should eventually start failing Puppet builds, and eventually be
# removed altogether.
class base::kerberos::dr inherits base::kerberos {
}
