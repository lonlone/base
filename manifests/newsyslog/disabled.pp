# This class sets up just enough of the machinery of newsyslog so that other
# classes don't complain that it's missing, but disables it so that all log
# rotation can be handled with logrotate.  Some other classes depend on the
# Package['newsyslog'] resource to ensure that directories exist, so we need
# to provide it.

class base::newsyslog::disabled {
  package { 'newsyslog': ensure => present; }

  # We can leave /etc/newsyslog.* directories in place once the cron jobs are
  # removed, since nothing will look at them.
  file {
    '/etc/cron.daily/newsyslog':   ensure => absent;
    '/etc/cron.weekly/newsyslog':  ensure => absent;
    '/etc/cron.monthly/newsyslog': ensure => absent;
  }
}
