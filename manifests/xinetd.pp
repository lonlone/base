# Our default inetd system, used by both Red Hat and Debian.

class base::xinetd {
    # Make sure conflicting inetd packages are purged
    package {
      'openbsd-inetd':
        ensure  => absent;
      'xinetd':
        ensure  => present,
        require => Package['openbsd-inetd'];
    }

    # Ensure service is running.  We have a custom restart action because we
    # had problems with Puppet starting or restarting xinetd with a broken
    # environment containing debconf environment variables and other things.
    # The below ensures that it restarts with a clean environment.
    $path = '/bin:/usr/bin:/sbin:/usr/sbin'
    service { 'xinetd':
        ensure    => running,
        name      => 'xinetd',
        enable    => true,
        hasstatus => false,
        status    => 'pidof xinetd',
        restart   => "/usr/bin/env - PATH=$path /etc/init.d/xinetd restart",
        require   => Package['xinetd'],
    }

    # Purge all configs not provided by puppet.
    file { '/etc/xinetd.d':
        ensure  => directory,
        purge   => true,
        recurse => true,
        notify  => Service['xinetd'],
        require => Package['xinetd'],
    }

    # Point people looking at the wrong file to the right place.
    if ($::osfamily == 'Debian') {
        file { '/etc/inetd.conf':
            content => "# Disabled, using xinetd instead\n",
        }
    }

    # Install the filter-syslog rules.
    file { '/etc/filter-syslog/xinetd':
        source => 'puppet:///modules/base/xinetd/etc/filter-syslog/xinetd',
    }
}

# Disable xinetd if don't want (x)inetd running
class base::xinetd::disabled {
    service {'xinetd':  ensure => stopped }
}
