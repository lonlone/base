#
# Sets up basic PAM configuration for Debian, separated out from the original
# kerberos configuration.

class base::pam::debian(
  $ensure = 'present',
){
  if ($ensure == 'present') {
    package { 'libpam-krb5': ensure => present }
    package { 'libpam-afs-session': ensure => present }

    # Starting with Debian jessie, pam-auth-update manages the common PAM files.
    if ($::lsbmajdistrelease < 8) {
      file {
        '/etc/pam.d/common-auth':
          source  => 'puppet:///modules/base/pam/etc/pam.d/common-auth',
          require => [ Package['libpam-afs-session'],
                       Package['libpam-krb5'] ];
       '/etc/pam.d/common-account':
          source  => 'puppet:///modules/base/pam/etc/pam.d/common-account',
          require => [ Package['libpam-krb5'] ];
       '/etc/pam.d/common-session':
          source  => 'puppet:///modules/base/pam/etc/pam.d/common-session',
          require => [ Package['libpam-afs-session'],
                       Package['libpam-krb5'] ];
      }
    }
  } elsif ($ensure == 'absent') {
    package { 'libpam-krb5':        ensure => absent }
    package { 'libpam-afs-session': ensure => absent }

    # Starting with Debian jessie, pam-auth-update manages the common PAM files.
    if ($::lsbmajdistrelease < 8) {
      file { '/etc/pam.d/common-auth':
        ensure => absent
      }
      file { '/etc/pam.d/common-account':
        ensure => absent
      }
      file {'/etc/pam.d/common-session':
        ensure => absent
      }
    }
  } else {
    fail("ensure parameter must be either 'present' or 'absent'")
  }
}

