# This class creates the file /etc/puppet/puppet-db.ini containing the
# MySQL stored config database connection credentials. These credentials
# are used both by the frankozes and the jimhensons.
#
# The values in /etc/puppet/puppet-db.ini are used in the puppet.conf file
# on all the puppet servers as well as by the malkovich web
# application. Thus, it must be downloaded and up-to-date before those
# other files can use it.
#
# See also the file init.pp in this directory.

class base::puppetclient::db_credentials {

  # This is where we put the db credentials.
  $puppet_db_ini_file = '/etc/puppet/puppet-db.ini'

  if ($::hostname =~ /^jimhenson-dev/) {
    $credentials_wallet_name = 'idg-puppet-dev-db-isat'
  }
  elsif ($::hostname =~ /^frankoz[12]-dev/) {
    $credentials_wallet_name = 'idg-puppet-dev-db-isat'
  }
  elsif ($::hostname =~ /^frankoz[12]/) {
    $credentials_wallet_name = 'idg-puppet-prod-db-isat'
  }
  elsif ($::hostname =~ /^jimhenson\d/) {
    $credentials_wallet_name = 'idg-puppet-prod-db-isat'
  }
  elsif ($::hostname =~ /^(puppetservice.*dev|puppetrepo-dev|puppetdb-dev)/) {
    $credentials_wallet_name = 'idg-puppetdb-dev-db-isat'
  }
  elsif ($::hostname =~ /^puppetservice\d$/) {
    $credentials_wallet_name = 'idg-puppetdb-prod-db-isat'
  }
  else {
    fail("cannot create puppet db credentials file for server $::hostname")
  }

  base::wallet { $credentials_wallet_name:
    type  => 'file',
    path  => $puppet_db_ini_file,
    mode  => '0640',
    owner => 'root',
    group => 'root',
  }
}
