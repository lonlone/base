#
# Use:
#
#   base::puppetclient::apply_credentials { '/etc/puppet/puppet.conf':
#     template => '/etc/puppet/puppet.conf.template',
#   }
#

define base::puppetclient::apply_credentials (
  $template = 'NONE',
  $target   = $name,
  $owner    = 'root',
  $group    = 'root',
  $mode     = '0640',
) {

  if ($template == "NONE") {
    fail "cannot call base::puppetclient::apply_credentials without a template value"
  }

  # Need the database credentials.
  include base::puppetclient::db_credentials
  $config = $puppetclient::db_credentials::puppet_db_ini_file

  ## Put template together with DB credentials
  exec { "generate-conf $target":
    command     => "generate-conf --template $template --config $config --newfile $target",
    refreshonly => true,
    subscribe   => [
                     File[$template],
                     Base::Wallet[$puppetclient::db_credentials::credentials_wallet_name],
                   ]
  }

  # Set file permissions
  file { $target:
    owner   => $owner,
    group   => $group,
    mode    => $mode,
    require => Exec["generate-conf $target"],
  }

}