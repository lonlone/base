# This class setups up puppetclient with a run interval of once every two
# hours.
class base::puppetclient::infrequent inherits base::puppetclient {
  Base::Puppetclient::Config['/etc/puppet/puppet.conf'] {
    runinterval => 7200,
  }
}
