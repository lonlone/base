# Change the time that daily cron jobs run, primarily so that log periods will
# be close to calendar days.  Also remove anacron, since we don't use it on
# servers.

# allow_install_anacron: if you want to allow the anacron package to be
# installed along with cron, set the parameter to true. This is useful,
# e.g., for Ubuntu as the ubuntu-desktop package depends on anacron.
#
# Note: setting this to true does not install the anacron package, rather
# it simply allows it to be installed.
#
# Default: false.
#

class base::cron(
  $allow_install_anacron = false
) {

  if (!$allow_install_anacron) {
    package { 'anacron': ensure => absent }
  }

  if ($::osfamily == 'Debian') {
    $crond = 'cron'
  }
  elsif ($::osfamily == 'RedHat') {
    $crond = 'crond'
    if ($::lsbmajdistrelease == '6') {
      # OL6 appears to be stuck with cronie-anacron
      if ($::operatingsystem == 'OracleLinux') {
        package { 'cronie-anacron':   ensure => present }
      } else { # Otherwise, prefer cronie-noanacron
        package {
          'cronie-noanacron': ensure => present;
          'cronie-anacron':   ensure => absent;
        }
      }
    }
    file { [ '/etc/cron.d/dailyjobs', '/etc/cron.d/0hourly' ]:
      ensure => absent
    }
  }
  else {
    fail "Unknown operatingsystem in base::cron: ${::operatingsystem}"
  }

  service { $crond:
    ensure => running,
    enable => true,
  }

  file { '/etc/crontab':
    content => template('base/cron/crontab.erb'),
    notify  => Service[$crond],
  }
}
