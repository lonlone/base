# Set up some common Duo configuration.  Normally each host has its own Duo 
# key, but some hosts have multiple Duo keys (for example, one for the host and 
# one for an application running on the host).  So, we need a separate type 
# that can be called to pull the Duo credentials from Wallet.
#
# The Wallet object will be stored to /etc/security/pam_duo_$name.conf
# If you don't need to customize anything, then your code can use it directly, 
# otherwise your code will need to copy it, and then make changes as 
# appropriate.

define base::duo::config::common (
  $ensure = 'present',
) {
  # Validate $ensure
  if ($ensure != 'present') and ($ensure != 'absent') {
    fail('ensure may only be "present" or "absent"')
  }

  # Lowercase the name, and then pull the object from Wallet
  $wallet_name_downcase = downcase($name)

  # Bring in the Wallet object, or get rid of it!
  # Also, bring in the Duo package, as a convenience to the client.
  if ($ensure == present) {
    require base::duo::package

    base::wallet { $wallet_name_downcase:
      ensure  => present,
      type    => 'duo-pam',
      path    => "/etc/security/pam_duo_${name}.conf",
    }
  } else {
    file { "/etc/security/pam_duo_${name}.conf":
      ensure => absent,
    }
  }
}
