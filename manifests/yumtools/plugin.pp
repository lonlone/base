# Define: base::yumtools::plugin
#
# This definition installs Yum plugin.
#
# Parameters:
#   [*ensure*]   - specifies if plugin should be present or absent
#
# Actions:
#
# Requires:
#   RPM based system
#
# Sample usage:
#   yum::plugin { 'versionlock':
#     ensure  => present,
#   }
#
define base::yumtools::plugin (
  $ensure     = present,
  #$pkg_prefix = 'yum-plugin',
  $pkg_name   = ''
) {
  
  $pkg_prefix = $lsbmajdistrelease ? {
      '5'     => 'yum',
      default => 'yum-plugin',
  }
  $_pkg_name = $pkg_name ? {
    ''      => "${pkg_prefix}-${name}",
    default => "${pkg_prefix}-${pkg_name}"
  }

  package { $_pkg_name:
    ensure  => $ensure,
  }

}
