# Parameterized class to lock puppet and facter versions
# on RHEL-ish hosts using yum versionlock
#    facter_version: the yum-aware version of facter to lock
#    puppet_version: the yum-aware version of puppet to lock
class base::yumtools::yum_puppet_lock (
  $facter_version = '2.2.0-1',
  $puppet_version = '3.7.2-1'
) {
  base::yumtools::versionlock {
    "0:puppet-${puppet_version}.el${::lsbmajdistrelease}*":
      ensure => present,
  }
  base::yumtools::versionlock {
    "1:facter-${facter_version}.el${::lsbmajdistrelease}*":
      ensure => present,
  }
}
