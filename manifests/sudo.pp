# Installs sudo and, optionally, enables Duo for sudo.

# $duo: enable pam_duo for sudo. Defaults to false.
#
# $duo_sudoers: A list of users that are allowed to call sudo.
# Defaults to the empty array.  Only used when $duo is set to true.
#
# $duo_fail_secure: A boolean, normally false.  If set to false, a Duo 
# timeout will cause the Duo step to be skipped.  If set to true, a Duo timeout 
# will cause the Duo step to fail (which means sudo will be blocked).  Only 
# used when $duo is set to true.
#
# $duo_gecos: A boolean, normally true.  If false, then Duo will will use the 
# user's username.  If true, then Duo will use the contents of the user's GECOS 
# field as their username.  This is important if you are using an alternate 
# account.
#
# $timeout: how long (in minutes) between requiring a new Duo re-auth.
# Default: 30
#
# $debuild: set this true if you need to set up a debuild environment.
# Default: false
#
# Example.
# To install sudo with no Duo support:
#
#   include base::sudo
#
# Example.
# To install sudo WITH Duo support
#
#   class { 'base::sudo':
#     duo         => true,
#     duo_sudoers => ['adamhl', 'yuelu'],
#   }
#
# Example.
# To install sudo WITH Duo support and require Duo auths
# after 4 minutes.
#
#   class { 'base::sudo':
#     duo         => true,
#     duo_sudoers => ['adamhl', 'yuelu'],
#     timeout     => 4,
#   }

class base::sudo(
  $duo             = false,
  $duo_sudoers     = [],
  $duo_fail_secure = false,
  $duo_gecos       = true,
  $timeout         = 30,
  $debuild         = false,
){
  # Install the sudo package
  package { 'sudo':
    ensure => installed
  }

  # Configure a default timeout
  file_line { 'Set sudo timeout':
    ensure => present,
    path   => '/etc/sudoers',
    line   => "Defaults  timestamp_timeout = $timeout",
    match  => '^Defaults\ +timestamp_timeout',
  }

  # If duo is enabled, require base::duo and set up the sudoers file.
  if ($duo) {
    # Validate $duo_fail_secure and $duo_gecos
    # base::duo::config does this too, but by doing it here it is clearer to
    # clients where the problem is!
    if !is_bool($duo_fail_secure) {
      fail('base::sudo::duo_fail_secure must be true or false')
    }
    if !is_bool($duo_gecos) {
      fail('base::sudo::duo_gecos must be true or false')
    }

    # Install the Duo config, passing the GECOS and fail-secure settings through
    base::duo::config { '/etc/security/pam_duo_su.conf':
      ensure      => present,
      use_gecos   => $duo_gecos,
      fail_secure => $duo_fail_secure,
    }

    # Install the pam.d configuration that requires Duo on sudo.
    file {'/etc/pam.d/sudo':
      ensure  => present,
      content => template('base/sudo/etc/pam.d/sudo.erb'),
      require => Package['sudo'],
    }

    # Make sure that the sudoers.d directory exists, and is read
    # This is done on alot of OSes already, but not all.
    file { '/etc/sudoers.d':
      ensure  => directory,
      owner   => 'root',
      group   => 'root',
      mode    => '0750',
      require => Package['sudo'],
    }
    file_line { 'Add sudoers.d includedir':
      ensure  => present,
      path    => '/etc/sudoers',
      line    => "#includedir /etc/sudoers.d",
      require => File['/etc/sudoers.d'],
    }

    # Install the suoders file. This takes the array $duo_sudoers
    # and puts it into /etc/sudoers.d/duo
    file {'/etc/sudoers.d/duo':
      ensure  => present,
      content => template('base/sudo/etc/sudoers.d/duo.erb'),
      require => File_line['Add sudoers.d includedir']
    }
  }

  # If we're not using Duo, we still might need to update the sudoers file
  else {
    # Debian's config is fine, but we need to enable wheel in RHEL systems
    if $::osfamily == 'RedHat' {
      file_line { 'Enable wheel sudoers':
        ensure => present,
        path   => '/etc/sudoers',
        line   => '%wheel        ALL=(ALL)       ALL',
        match  => '# %wheel        ALL=(ALL)       ALL',
      }
    }
  }
}
