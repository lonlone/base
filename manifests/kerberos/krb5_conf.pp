# A define that creates a krb5.conf file for use with the stanford.edu realm.
#
# INTRODUCTION
# This defined resource type generates a krb5.conf file suitable for use
# on a Stanford Kerberos client. It should _not_ be used on a KDC; KDC's
# should use the kdc class instead.
#
# This defined resource type allows a great deal of customization, but it
# can be used to generate the default production krb5.conf file.
#
# EXAMPLES
#
# #######################################################################
# 1. To generate the standard /etc/krb5.conf for use in the production
# stanford.edu environment:
#
#   base::kerberos::krb5_conf { '/etc/krb5.conf': }
#
# That's it!
#
# #######################################################################
# 2. If you need a krb5.conf for use with one of the non-production
# environments, use the 'env' parameter. For example, to generate the
# krb5.conf file suitable for authentication against the 'test' Kerberos
# environment:
#
#   base::kerberos::krb5_conf { '/etc/krb5.conf':
#     env => 'test',
#   }
#
# This will generate a krb5.conf file whose [realms] section contains this
# definition for stanford.edu:
#
#   [realms]
#       stanford.edu = {
#           kdc            = krb5auth-<env>1.stanford.edu:88
#           kdc            = krb5auth-<env>2.stanford.edu:88
#           kdc            = krb5auth-<env>3.stanford.edu:88
#           master_kdc     = master-kdc-<env>.stanford.edu:88
#           admin_server   = krb5-admin-<env>.stanford.edu
#           kpasswd_server = krb5-admin-<env>.stanford.edu
#           default_domain = stanford.edu
#           kadmind_port   = 749
#       }
#
# #######################################################################
# 3. You can do complete customization by setting the 'env' to the value
# 'custom':
#
#   base::kerberos::krb5_conf { '/etc/krb5.conf':
#     env  => 'custom',
#     kdcs => [
#       'kerberos-abc1.stanford.edu',
#       'kerberos-abc2.stanford.edu',
#     master_kdc     => 'master-kdc-abc.stanford.edu',
#     admin_server   => 'kerberos-abc1.stanford.edu',
#     kpasswd_server => 'kerberos-abc1.stanford.edu',
#   }
#
#
#
# PARAMETERS
#
## BASIC
#
# The $name parameter should be the full path to where the file will be put.
#
# $prefer_tcp:
#   Normal kerberos traffic uses UDP, but some applications
#   (lookin' at you Java!) work better with TCP. Set this parameter to
#   "true" to force the client to prefer TCP to UDP.
#   Default: false
#
# $rdns_enabled:
#   If 'true' have the Kerberos client do a reverse DNS lookup on the
#   hostname when connecting to a server. This should be set to 'false' if
#   you want the client to be able to connect to services where the service
#   name's IP address PTR record may not match the hostname (e.g., for
#   services running in Amazon Web Services).
#   Default: false
#
## ADVANCED
#
# $env: Valid values:
#  * prod (default)
#  * dev
#  * test
#  * uat
#  * qa
#  * custom
#
# In the "stanford.edu" section of [realms], by default the production
# settings will appear:
#
#   [realms]
#       stanford.edu = {
#           kdc            = krb5auth1.stanford.edu:88
#           kdc            = krb5auth2.stanford.edu:88
#           kdc            = krb5auth3.stanford.edu:88
#           master_kdc     = master-kdc.stanford.edu:88
#           admin_server   = krb5-admin.stanford.edu
#           kpasswd_server = krb5-admin.stanford.edu
#           default_domain = stanford.edu
#           kadmind_port   = 749
#       }
#
# If the environment is set to a different value, then that section will
# instead look like this:
#
#   [realms]
#       stanford.edu = {
#           kdc            = krb5auth-<env>1.stanford.edu:88
#           kdc            = krb5auth-<env>2.stanford.edu:88
#           kdc            = krb5auth-<env>3.stanford.edu:88
#           master_kdc     = master-kdc-<env>.stanford.edu:88
#           admin_server   = krb5-admin-<env>.stanford.edu
#           kpasswd_server = krb5-admin-<env>.stanford.edu
#           default_domain = stanford.edu
#           kadmind_port   = 749
#       }
#
# For example, if $env is set to 'test', then the above would be
#
#   [realms]
#       stanford.edu = {
#           kdc            = krb5auth-test1.stanford.edu:88
#           kdc            = krb5auth-test2.stanford.edu:88
#           kdc            = krb5auth-test3.stanford.edu:88
#           master_kdc     = master-kdc-test.stanford.edu:88
#           admin_server   = krb5-admin-test.stanford.edu
#           kpasswd_server = krb5-admin-test.stanford.edu
#           default_domain = stanford.edu
#           kadmind_port   = 749
#       }
#
#
# Finally, if you want to override these using these parameters, set the
# $env variable to 'custom' and set these parameters:
#
#
# $kdcs: Use this set of server names for the "kdc" entries in the
#   realm. If the array is empty, use the the normal production KDC list.
#
# Example:
#  kdcs => ['kerberos-qa2.stanford.edu', 'kerberos-qa1.stanford.edu'],
#
# will result in
#
# [realms]
#   stanford.edu = {
#     kdc            = kerberos-qa2.stanford.edu:88
#     kdc            = kerberos-qa1.stanford.edu:88
#
# $master_kdc: sets the master_kdc setting.
#
# $admin_server: sets the admin_server setting
#
# $kpasswd_server: sets the kpasswd_server setting.
#
# $krb5_port: sets the port number on the kdc and master-kdc entries.
#   Default: 88
#
# NOTE! If $env is set to 'custom', then ALL of $kdcs, $master_kdc,
# $admin_server, and $kpasswd_server MUST be set. If not, Puppet will
# raise an exception.


define base::kerberos::krb5_conf (
  $env                             = 'prod',
  $realm                           = 'stanford.edu',
  $default_realm                   = 'stanford.edu',
  $stanford_realm_is_production    = true,
  $kdcs                            = [],
  $master_kdc                      = undef,
  $admin_server                    = undef,
  $kpasswd_server                  = undef,
  $krb5_port                       = '88',
  $rdns_enabled                    = false,
  $prefer_tcp                      = false,
) {

  case $env {
    'prod': {
      $kdcs_actual = [
        "krb5auth1.stanford.edu",
        "krb5auth2.stanford.edu",
        "krb5auth3.stanford.edu",
      ]
      $master_kdc_actual     = "master-kdc.stanford.edu"
      $admin_server_actual   = "krb5-admin.stanford.edu"
      $kpasswd_server_actual = "krb5-admin.stanford.edu"
    }
    'dev', 'test', 'uat', 'qa': {
      $kdcs_actual = [
        "krb5auth-${env}1.stanford.edu",
        "krb5auth-${env}2.stanford.edu",
        "krb5auth-${env}3.stanford.edu",
      ]
      $master_kdc_actual     = "master-kdc-${env}.stanford.edu"
      $admin_server_actual   = "krb5-admin-${env}.stanford.edu"
      $kpasswd_server_actual = "krb5-admin-${env}.stanford.edu"
    }
    'custom': {
      # Verify that the needed parameters are set.
      if (size($kdcs) == 0) {
        fail("when using a 'custom' environment you must define the kdc's")
      }
      if ($master_kdc == undef) {
        fail("when using a 'custom' environment you must define the master_kdc")
      }
      if ($admin_server == undef) {
        fail("when using a 'custom' environment you must define the admin_server")
      }
      if ($kpasswd_server == undef) {
        fail("when using a 'custom' environment you must define the kpasswd_server")
      }
      $kdcs_actual           = $kdcs
      $master_kdc_actual     = $master_kdc
      $admin_server_actual   = $admin_server
      $kpasswd_server_actual = $kpasswd_server
    }
    default : {
      fail("do not know env '${env}'")
    }
  }

  file { $name:
    content => template('base/kerberos/etc/krb5.conf.erb'),
  }
}
