# Configuration used on VMware guests.

# $use_tripwire: if set to true will include the tripwire client
#  class.
#  Default: true

class base::vmguest(
  $use_tripwire = true
) {
  include base::vmguest::syslog

  if ($use_tripwire) {
    include base::vmguest::tripwire
  }

  # Install filter-syslog rules to ignore vmware-tools noise.
  file { '/etc/filter-syslog/vmguest':
    source  => 'puppet:///modules/base/vmguest/etc/filter-syslog/vmguest';
  }

  case $::osfamily {
    'Debian': {
      $open_vm_module = $::lsbdistcodename ? {
        'lenny'   => "open-vm-modules-${::kernelrelease}",
        'squeeze' => "open-vm-modules-${::kernelrelease}",
        'precise' => 'open-vm-dkms',
        'raring'  => 'open-vm-dkms',
        'wheezy'  => 'open-vm-dkms',
        'jessie'  => 'open-vm-tools-dkms',
        default   => 'open-vm-tools-dkms',
      }
      package {
        'open-vm-tools': ensure => installed;
        $open_vm_module: ensure => installed;
      }

      # udev rules for SCSI I/O timeout.  Needs config on older Debian since
      # open-vm-tools doesn't modify udev like VMwareTools on RHEL.
      if $::lsbdistcodename in ['lenny', 'squeeze'] {
        file { '/etc/udev/rules.d/99-vmware-scsi-udev.rules':
          source => 'puppet:///modules/base/vmguest/etc/udev/rules.d/99-vmware-scsi-udev.rules',
        }
      }
    }
    'RedHat': {
      # we do not the rebundled version by this name
      package { 'VMwareTools': ensure => absent }
      # VMWare recommends open-vm-tools for EL7
      if ($::lsbmajdistrelease != '7') {
        base::rpm::yumrepo { "vmware-tools-EL${::lsbmajdistrelease}.repo": }
        base::rpm::import {
          'vmware-tools-dsa':
            url       =>
              'http://yum.stanford.edu/VMWARE-PACKAGING-GPG-DSA-KEY.pub',
              signature => 'gpg-pubkey-04bbaa7b-4c881cbf';
          'vmware-tools-rsa':
            url       =>
              'http://yum.stanford.edu/VMWARE-PACKAGING-GPG-RSA-KEY.pub',
              signature => 'gpg-pubkey-66fd4949-4803fe57';
        }
        package { 'vmware-tools-esx-nox':
            ensure  => present,
            require => [
              Base::Rpm::Yumrepo["vmware-tools-EL${::lsbmajdistrelease}.repo"],
              Base::Rpm::Import['vmware-tools-dsa'],
              Base::Rpm::Import['vmware-tools-rsa'],
              Package['VMwareTools']
              ],
        }
      } else { # EL7-only, since VMWare does not package vmware-tools* for it
        package { 'open-vm-tools':
          ensure  => installed,
          require => Package['VMwareTools'],
        }
      }
      # for all EL releases
      service { 'smartd':
        ensure => stopped,
        enable => false,
      }
    }
  }
}
