#
# Module for setting up subversion server

# Can't use a module name with the same name as a node on that node, so the
# subversion.stanford.edu server has to include the two parts separately.
class base::subversion {
    include base::subversion::files,
            base::subversion::packages
}

class base::subversion::files {
    file {
        "/srv":
            ensure => directory,
            mode   => 755;
        "/srv/svn":
            ensure => directory,
            mode   => 2775;
        "/srv/svn/backups":
            ensure => directory,
            mode   => 750;
    }
}

class base::subversion::packages {
    package {
        "cvs2svn":          ensure => installed;
        "diffstat":         ensure => installed;
        "subversion":       ensure => installed;
        "subversion-tools": ensure => installed;
        "svnlog":           ensure => installed;
    }
}