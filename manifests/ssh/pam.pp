# Install /etc/pam.d/sshd.

# If $pam_duo is set to true, use a pam stack that requires Duo for
# regular logins.
#
# Currently, only Debian is supported when $pam_duo is true.
#
# If you are using the SLURM job scheduler, setting $pam_slurm to true will
# cause user logins to be rejected unless they have a valid job allocation.
# In that case, you can set $pam_slurm_bypass to an absolute path, where all
# users listed in the file (one username per line) will not be checked.  This
# is good so that admin users can continue to log in.

class base::ssh::pam (
  $pam_afs          = true,
  $pam_duo          = false,
  $pam_slurm        = false,
  $pam_slurm_bypass = 'NONE',
){

  # Configure PAM for sshd on RHEL 6.
  if ($::lsbdistcodename == 'santiago') {
    file { '/etc/pam.d/sshd':
      ensure => link,
      target => '/etc/pam.d/system-auth',
    }
  } elsif ($pam_duo) {
    if ($::osfamily =~ /Debian/) {
      file {'/etc/pam.d/sshd':
        ensure => present,
        content => template('base/ssh/etc/pam.d/sshd.erb'),
      }
    } else {
      fail("cannot call ssh::pam with pam_duo true under OS '$::osfamily'")
    }
  }
}
