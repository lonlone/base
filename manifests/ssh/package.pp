# Load the openssh package. This is a bit tricky depending on the version
# of the OS and whether or not we are using pam_duo.

class base::ssh::package (
  $pam_duo = false
){

  if ($pam_duo) {
    # Make sure sshd (at least version 6.2 is installed)
    case $::operatingsystem {
      'Debian': {
        $sshd_package = 'openssh-server'

        # Debian wheezy is the oldest supported version
        if ($::lsbmajdistrelease < 7) {
          fail('pam_duo requires at least Debian wheezy')
        }
        elsif ($::lsbmajdistrelease == 7) {
          # On wheezy, pin the backported openssh
          file { '/etc/apt/preferences.d/openssh':
              ensure => present,
              source => 'puppet:///modules/base/ssh/etc/apt/preferences.d/openssh.wheezy',
          }
        }
      }

      'Ubuntu': {
        $sshd_package = 'openssh-server'

        # Ubuntu trusty is the oldest supported version
        if ($::lsbmajdistrelease < 14) {
          fail('pam_duo requires at least Ubuntu trusty')
        }
      }

      # NOTE: The templates have not been fully-validated on RHEL-type systems,
      # so pam_duo will still fail on them (just not in this section)
      'RedHat', 'CentOS': {
        $sshd_package = 'openssh-server'

        # RHEL/CentOS 7 is the oldest supported version
        if ($::lsbmajdistrelease < 7) {
          fail('pam_duo requires at least RHEL/CentOS 7')
        }
      }

      default: {
        fail('pam_duo is not supported on this OS.')
      }
    }

    # Actually install the package with sshd now
    if !defined( Package[$sshd_package] ) {
      package { $sshd_package:
        ensure => 'installed',
      }
    } else {
      # On Debian/Ubuntu, manually run aptitude to upgrade openssh-server
      if ($::osfamily == 'Debian') {
        exec { 'update ssh':
          command     => 'aptitude -y install openssh-server',
          path        => '/usr/bin',
          provider    => shell,
          refreshonly => true,
          require     => Exec['aptitude_update_for_ssh'],
        }

        exec {'aptitude_update_for_ssh':
          command => 'aptitude update',
          path    => '/usr/bin',
        }
      }
    }
  } else {
    package { 'openssh-server': ensure => present }
  }


}
