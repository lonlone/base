# Allow both public key authentication and host-based key authentication.  (We
# don't have any systems that need host-based keys but not public key
# authentication.)

class base::ssh::hostbased inherits base::ssh::pubkey {
  if ($::osfamily == 'RedHat') and ($::lsbmajdistrelease >= 6) {
    fail("ssh::new::hostbased should not be used with RHEL 6 or later")
  }

  # Override the sshd configuration to enable pubkey.
  Base::Ssh::Config::Sshd['/etc/ssh/sshd_config'] { hostbased => true }
}
