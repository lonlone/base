# Rename the tripwire cron job so that it runs last.  This can be slow on VMs,
# plus this version of the cron job randomizes the start time.

class base::vmguest::tripwire inherits base::tripwireclient {
  File['/etc/cron.daily/tripwire'] { ensure => absent }
  file { '/etc/cron.daily/zztripwire':
    ensure => present,
    mode   => '0755',
    owner  => 'root',
    source => 'puppet:///modules/base/vmguest/etc/cron.daily/zztripwire';
  }
}