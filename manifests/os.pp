# General configuration of the basic operating system.  Nearly everything
# here is specific to the version of Linux we're running and is handled by
# the appropriate separate class.
class base::os {

  case $::operatingsystem {
    'Debian':      { include base::os::debian }
    'Ubuntu':      { include base::os::ubuntu }
    'RedHat':      { include base::os::redhat }
    'CentOS':      { include base::os::centos }
    'OracleLinux': { include base::os::oraclelinux }
    default:       { include base::os::debian }
  }

  base::os::motd { '/etc/motd':
    ensure   => present,
    template => 'base/os/motd.erb',
  }

  # Get warranty expiration facts for Dell hardware.
  if ($::manufacturer =~ /Dell/) {
    file {
      '/etc/cron.d/dell-warranty-facts':
        source => 'puppet:///modules/base/os/etc/cron.d/dell-warranty-facts';
      '/var/lib/puppet/sufact/su_dell_warranty_days':
        ensure => present;
      '/var/lib/puppet/sufact/su_dell_warranty_end':
        ensure => present;
    }
    include packages::curl
  }

  # Generate an iptables fact for the firewall team to query.
  file { '/var/lib/puppet/sufact/su_iptables':
    ensure => $::osfamily ? {
        'RedHat' => '/etc/sysconfig/iptables',
        default  => '/etc/iptables/general',
    }
  }

}
