# This class adds a reference to a local DNS caching server.
#
# NOTE: This class only works with Debian.
#
# $cache_size_entries: the number of entries to cache.
#   Default: 10000

class base::dns::cache(
  $cache_size_entries = 10000
) inherits base::dns {

  if ($::osfamily != 'Debian') {
    fail 'base::dns::cache_packages only works with Debian'
  }

  Base::Dns::Resolv_conf[$::fqdn_lc] { dns_cache => true }

  # We use stanford-dnscache for wheezy and dnsmasq for jessie and later.
  if ($::lsbdistcodename == 'wheezy') {
    # WHEEZY

    include base::daemontools

    # Stanford package for DNS caching.
    package { 'stanford-dnscache':
      ensure => present,
      notify => Exec['chown dnslog:dnslog /var/log/dnscache'],
    }

    # This is required because ownership is wrong on reinstall of packages
    # but not on initial install.
    exec {'chown dnslog:dnslog /var/log/dnscache':
      command     => 'chown -R dnslog:dnslog /var/log/dnscache',
      refreshonly => true,
    }
  } else {
    # JESSIE and beyond

    package { 'dnsmasq':
      ensure => present,
    }

    # Call the template for /etc/dnsmasq.d/stanford-dnscache.conf. This
    # template uses the $cache_size_entries class parameter.
    file {'/etc/dnsmasq.d/stanford-dnscache.conf':
      content => template('base/dns/etc/dnsmasq.d/stanford-dnscache.conf.erb'),
      require => Package['dnsmasq'],
      notify  => Service['dnsmasq'],
    }

    # Define the service and make sure it runs. Restart the service if
    # /etc/resolv.conf changes.
    service { 'dnsmasq':
      ensure    => 'running',
      require   => Package['dnsmasq'],
      subscribe => File['/etc/resolv.conf'],
    }
  }

}
