# These classes are for servers at Livermore.  The resolv.conf puts
# the Livermore dns server first in the search list.
class base::dns::dr inherits base::dns {
  Base::Dns::Resolv_conf[$::fqdn_lc] { first_dns_server => '204.63.227.68' }
}
