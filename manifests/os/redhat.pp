#
# Rules specific to Red Hat systems.  Try to keep this rule set to an
# absolute minimum.  Part of the goal of Puppet is to make our systems
# look as similar as possible given the inherent differences between the
# distributions, and that means that changes should be wrapped in
# conceptual packages that do equivalent things on both RHEL and Debian.
# This ruleset is mostly generic enough for use by CentOS, and maybe other
# RHEL-like operating systems. If larger edits are necessary for future
# OS', fork this off into another manifest rather than edit and include it

# The newsyslog module provides the /etc/filter-syslog directory, which we
# need to install rules.
class base::os::redhat {
  include base::newsyslog,
          base::os::redhat::syslog,
          base::rpm,
          epel # our partial EPEL mirror

  package {
      'emacs-nox':       ensure => present;
      'kstart':
        ensure  => present,
        require => Base::Rpm::Import['stanford-rpmkey'];
      'libxml2':   ensure  => present; # needed for dell firmware updates
      'mailx':     ensure  => present;
      'procmail':  ensure  => present; # needed for dell firmware updates
      'redhat-lsb': ensure => present;
  }

  base::rpm::import { 'stanford-rpmkey':
    url       => 'http://yum.stanford.edu/STANFORD-GPG-KEY',
    signature => 'gpg-pubkey-af476543-44720559';
  }

  # RHEL RPM GPG Key stuff
  case $::lsbmajdistrelease {
    # RHEL4
    '4': {
      # CentOS repo handled in centos.pp
      if ($::operatingsystem == 'RedHat') {
        base::rpm::import { 'redhat-rpmkey':
          url       => '/usr/share/rhn/RPM-GPG-KEY',
          signature => 'gpg-pubkey-db42a60e-37ea5438';
        }
      }
      package {
        'kernel-utils': ensure => present;
        'slocate':      ensure => present;
        'lsb-release':  ensure => present;
      }
    }

    # RHEL5+
    default: {
      # CentOS repo handled in centos.pp
      if ($::operatingsystem == 'RedHat') {
        base::rpm::import { 'redhat-rpmkey':
          url       => '/etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release',
          signature => $::lsbmajdistrelease ? {
            '5' => 'gpg-pubkey-37017186-45761324',
            '6' => 'gpg-pubkey-fd431d51-4ae0493b',
          };
        }
      }
      if ($::lsbmajdistrelease != '7') {
        base::rpm::import { 'dag-rpmkey':
          url       =>
                '/usr/share/doc/rpmforge-release-0.3.6/RPM-GPG-KEY-rpmforge-dag',
          signature => 'gpg-pubkey-6b8d79e6-3f49313d';
        }
      }
      package {
        'mlocate': ensure => present;
      }
    }
  }

  # EL7 has its own class
  if ($::lsbmajdistrelease == '7') {
    include base::os::redhat::el7
  }

  # RHEL4 and 5 need links to krb utils
  case $::lsbmajdistrelease {
    '4','5': {
      file {
        '/usr/bin/kinit':
          ensure  => link,
          target  => '/usr/kerberos/bin/kinit';
        '/usr/bin/kdestroy':
          ensure  => link,
          target  => '/usr/kerberos/bin/kdestroy';
        '/usr/bin/ksu':
          ensure  => link,
          target  => '/usr/kerberos/bin/ksu';
      }
    }
    # RHEL6 just needs a link for fdformat since it moved to /usr/sbin
    '6': {
      file { '/usr/bin/fdformat':
        ensure  => link,
        target  => '/usr/sbin/fdformat';
      }
      package { 'redhat-lsb-core': ensure => present }
    }
    default: {}
  }

  file {
    '/etc/filter-syslog/redhat':
      source  => 'puppet:///modules/base/os/etc/filter-syslog/redhat',
      require => Package['filter-syslog'];
    '/etc/profile.d/usrlocal.sh':
      source  => 'puppet:///modules/base/os/etc/profile.d/usrlocal.sh';
    '/etc/profile.d/usrlocal.csh':
      source  => 'puppet:///modules/base/os/etc/profile.d/usrlocal.csh';
    '/etc/profile.d/krbsu.sh':
      source  => 'puppet:///modules/base/os/etc/profile.d/krbsu.sh';
    '/etc/profile.d/krbsu.csh':
      source  => 'puppet:///modules/base/os/etc/profile.d/krbsu.csh';
    '/etc/profile.d/prompt.sh':
      source  => 'puppet:///modules/base/os/etc/profile.d/prompt.sh';
    '/etc/sysconfig/selinux':
      ensure  => link,
      target  => '/etc/selinux/config';
    '/etc/selinux':
      ensure  => directory;
    '/etc/selinux/config':
      source  => 'puppet:///modules/base/os/etc/selinux/config';
    '/etc/cron.daily/prelink':
      ensure  => absent;
  }

  # Set kernel.sysrq = 1 for RH systems
  base::sysctl { 'kernel.sysrq': ensure => 1 }

  # Disable zeroconf
  base::textline { 'NOZEROCONF=yes': ensure => '/etc/sysconfig/network' }

}
