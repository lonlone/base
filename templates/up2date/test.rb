#!/usr/bin/ruby
# test the erb template with variables below

require 'erb'

template = File.readlines("sources.erb").to_s
message = ERB.new(template, 0, "%<>")

lsbdistrelease = "4"
# test added variables as undefined, false, nil, and true

jpackage17 = nil
useRHN = false
remi = false
epel = false
java5 = false
java6 = false
shib = true

results = message.result
puts results
