tinker panic 0
# ^^^^^^^This should be the first configuration item in the conf file!
# See "NTP Recommendations"
# https://kb.vmware.com/selfservice/viewdocument.do?cmd=displayKC&docType=kc&externalId=1006427
#
# Always reset the clock, even if the new time is more than 1000s away
# from the current system time.  We need this on VMs and it shouldn't hurt
# anywhere else.  

# This is the default site ntpd configuration file.  It queries the three
# stratum-one time servers, which use GPS receivers for accurate time.  It also
# queries SRCC's stratum-one time server, located in SRCF, which also uses GPS.

# Listen to the stratum 2 pool.
server time-a.stanford.edu iburst
server time-b.stanford.edu iburst
server time-c.stanford.edu iburst
server srcf-ntp.stanford.edu iburst

# Save the clock drift.
driftfile /var/lib/ntp/ntp.drift

# Only talk to the network where the time servers are.
restrict 171.64.7.0 mask 255.255.255.0 nomodify
restrict 171.66.97.64 mask 255.255.255.192 nomodify
restrict 204.63.224.64 mask 255.255.255.192 nomodify

# Allow all settings from our localhost interface.
restrict 127.0.0.1
restrict -6 ::1

# Do not allow access from anybody else
restrict default ignore
restrict -6 default ignore

# Disable the monlist command, since it can be used for a UDP DoS attack.
disable monitor
